package xsis.latihan.services;

import java.util.List;

import xsis.latihan.controllers.Product;
import xsis.latihan.models.ProductModel;

public interface ProductService {
	
	List<ProductModel> findAllProduct();
	
	ProductModel save(ProductModel product);
	
	void delete(Long Id);

}
