package xsis.latihan.services;

import java.util.List;

import xsis.latihan.controllers.Category;
import xsis.latihan.models.CategoryModel;


public interface CategoryService {
	
	List<CategoryModel> findAllCategory();
	
	CategoryModel save(CategoryModel category);
	
	void delete(Long Id);

}
