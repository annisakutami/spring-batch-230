package xsis.latihan.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import xsis.latihan.models.CategoryModel;
import xsis.latihan.models.VariantModel;
import xsis.latihan.repositories.CategoryRepo;
import xsis.latihan.repositories.VariantRepo;

@Controller
@RequestMapping("/variant/")
public class Variant {
	
	@Autowired
	private VariantRepo variantrepo;
	@Autowired
	private CategoryRepo categoryrepo;
	
	@GetMapping("index")
	public  ModelAndView index() {
		ModelAndView view = new ModelAndView("/variant/index");
		List<VariantModel> variant = this.variantrepo.findAll();
		view.addObject("variant", variant);
		return view;
	}
	@GetMapping("form")
	public ModelAndView form() {
		ModelAndView view = new ModelAndView("/variant/form");
		VariantModel variantmodel = new VariantModel();
		List<CategoryModel> category = this.categoryrepo.findAll();
		view.addObject("category", category);
		view.addObject("variant", variantmodel);
		return view;
	}
	
	@PostMapping("save")
	public ModelAndView save(@ModelAttribute VariantModel variantmodel, BindingResult bindingresult) {
		if(bindingresult.hasErrors()) {
			return new ModelAndView("redirect:../index/");
		} else {
			this.variantrepo.save(variantmodel);
			return new ModelAndView("redirect:../index/");
		}
	}
	
	@GetMapping("edit/{id}")
	public ModelAndView edit(@PathVariable("id") Long id) {
		ModelAndView view = new ModelAndView("/variant/form");
		VariantModel variantmodel=this.variantrepo.findById(id).orElse(null);
		List<CategoryModel> category = this.categoryrepo.findAll();
		view.addObject("category", category);
		view.addObject("variant", variantmodel);
		return view;
	}
	
	@GetMapping("delete/{id}")
	public ModelAndView delete(@PathVariable("id") Long id) {
		ModelAndView view = new ModelAndView("/variant/delete");
		VariantModel varianttmodel=this.variantrepo.findById(id).orElse(null);
		view.addObject("variant", varianttmodel);
		return view;
	}
	
	@PostMapping("remove")
	public ModelAndView remove(@ModelAttribute VariantModel variantmodel, BindingResult bindingresult) {
		if(bindingresult.hasErrors()) {
			return new ModelAndView("redirect:../index/");
		} else {
			this.variantrepo.delete(variantmodel);
			return new ModelAndView("redirect:../index/");
		}
	}

}
