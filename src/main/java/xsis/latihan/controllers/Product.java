package xsis.latihan.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import xsis.latihan.models.CategoryModel;
import xsis.latihan.models.ProductModel;
import xsis.latihan.repositories.CategoryRepo;
import xsis.latihan.repositories.ProductRepo;

@Controller
@RequestMapping("/product/")
public class Product {
	
	@Autowired
	private ProductRepo productrepo;
	@Autowired
	private CategoryRepo categoryrepo;
	
	@GetMapping("index")
	public  ModelAndView index() {
		ModelAndView view = new ModelAndView("/product/index");
		List<ProductModel> product = this.productrepo.findAll();
		view.addObject("products", product);
		return view;
	}
	
	@GetMapping("form")
	public ModelAndView form() {
		ModelAndView view = new ModelAndView("/product/form");
		ProductModel productmodel = new ProductModel();
		List<CategoryModel> category = this.categoryrepo.findAll();
		view.addObject("category",category);
		view.addObject("products", productmodel);
		return view;
	}
	
	@PostMapping("save")
	public ModelAndView save(@ModelAttribute ProductModel productmodel, BindingResult bindingresult) {
		if(bindingresult.hasErrors()) {
			return new ModelAndView("redirect:../index/");
		} else {
			this.productrepo.save(productmodel);
			return new ModelAndView("redirect:../index/");
		}
	}
	
	@GetMapping("edit/{id}")
	public ModelAndView edit(@PathVariable("id") Long id) {
		ModelAndView view = new ModelAndView("/product/form");
		ProductModel productmodel=this.productrepo.findById(id).orElse(null);
		List<CategoryModel> category = this.categoryrepo.findAll();
		view.addObject("category",category);
		view.addObject("products", productmodel);
		return view;
	}
	
	@GetMapping("delete/{id}")
	public ModelAndView delete(@PathVariable("id") Long id) {
		ModelAndView view = new ModelAndView("/product/delete");
		ProductModel productmodel=this.productrepo.findById(id).orElse(null);
		view.addObject("products", productmodel);
		return view;
	}

	@PostMapping("remove")
	public ModelAndView remove(@ModelAttribute ProductModel productmodel, BindingResult bindingresult) {
		if(bindingresult.hasErrors()) {
			return new ModelAndView("redirect:../index/");
		} else {
			this.productrepo.delete(productmodel);
			return new ModelAndView("redirect:../index/");
		}
	}
	
	
}
