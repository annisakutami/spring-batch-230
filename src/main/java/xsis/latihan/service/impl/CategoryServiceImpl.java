package xsis.latihan.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import xsis.latihan.controllers.Category;
import xsis.latihan.models.CategoryModel;
import xsis.latihan.repositories.CategoryRepo;
import xsis.latihan.services.CategoryService;

@Service
public class CategoryServiceImpl implements CategoryService {
	
	@Autowired
	private CategoryRepo categoryRepo;

	@Override
	public List<CategoryModel> findAllCategory() {
		// TODO Auto-generated method stub
		return categoryRepo.findAll();
	}

	@Override
	public CategoryModel save(CategoryModel category) {
		// TODO Auto-generated method stub
		return categoryRepo.save(category);
	}

	@Override
	public void delete(Long Id) {
		// TODO Auto-generated method stub
		categoryRepo.deleteById(Id);
		
	}
	
	

}
