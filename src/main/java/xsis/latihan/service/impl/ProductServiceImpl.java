package xsis.latihan.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import xsis.latihan.controllers.Product;
import xsis.latihan.models.ProductModel;
import xsis.latihan.repositories.ProductRepo;
import xsis.latihan.services.ProductService;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductRepo productRepo;
	
	@Override
	public List<ProductModel> findAllProduct() {
		// TODO Auto-generated method stub
		return productRepo.findAll();
	}

	@Override
	public ProductModel save(ProductModel product) {
		// TODO Auto-generated method stub
		return productRepo.save(product);
	}

	@Override
	public void delete(Long Id) {
		// TODO Auto-generated method stub
		productRepo.deleteById(Id);
		
	}
	
	

}
